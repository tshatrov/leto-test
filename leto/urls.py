from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns(
    '',
    # Examples:
    # url(r'^$', 'leto.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),

    url(r'^$', 'poll.views.index', name='poll_index'),                       

    url(r'^api/driver-status/(?P<driver_id>\d+)/$', 'poll.views.api_driver_status',
        name='api_driver_status'),
    url(r'^api/driver-directions/$', 'poll.views.api_driver_directions',
        name='api_driver_directions'),

    url(r'^img/(?P<status>\w+)_(?P<action>\w+).png',
        'poll.views.gen_icon', name='gen_icon'),

    url(r'^timeline/(?P<driver_id>\d+)/$', 'poll.views.driver_timeline',
        name='driver_timeline'),

    url(r'^map/$', 'poll.views.driver_map',
        name='driver_map'),

)
