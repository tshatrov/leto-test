# -*- coding: utf-8 -*-

import json
import requests
import arrow

from django.core.management.base import CommandError, NoArgsCommand

from poll.models import DriverEvent

def parse_drivers_data(data):
    js = json.loads(data)
    fl = js['fittedLocations'][0]
    if fl and not DriverEvent.objects.filter(driver_id=js['driverId'], epoch=fl['epoch']).exists():
        obj= DriverEvent.objects.create(
            driver_id=js['driverId'],
            status=js['driverStatus'],
            timestamp=arrow.get(fl['timestamp']).datetime,
            course=fl.get('course'),
            epoch=fl['epoch'],
            horizontal_accuracy=fl.get('horizontalAccuracy'),
            longitude=fl['longitude'],
            latitude=fl['latitude'],
            altitude=fl.get('altitude'),
            speed=fl.get('speed'),
        )
        print obj

class Command(NoArgsCommand):
    url = 'http://task.uberleto.ru/poll/'

    def handle_noargs(self, **options):
        stream = requests.get(self.url, stream=True)
        lines = stream.iter_lines()
        mode = None
        for line in lines:
            print '> ' + line
            if line == '':
                mode = None
            elif ': ' in line:
                key, value = line.split(': ', 1)
                if key == 'event':
                    mode = value
                elif key == 'data' and mode == 'drivers':
                    parse_drivers_data(value)
                
