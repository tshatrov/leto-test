# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='DriverEvent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('driver_id', models.IntegerField(verbose_name='ID \u0432\u043e\u0434\u0438\u0442\u0435\u043b\u044f', db_index=True)),
                ('status', models.CharField(max_length=32, verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441 \u0432\u043e\u0434\u0438\u0442\u0435\u043b\u044f', db_index=True)),
                ('timestamp', models.DateTimeField(verbose_name='\u0412\u0440\u0435\u043c\u044f', db_index=True)),
                ('course', models.IntegerField(null=True, db_index=True)),
                ('epoch', models.IntegerField(db_index=True)),
                ('horizontal_accuracy', models.IntegerField()),
                ('longitude', models.FloatField(verbose_name='\u0414\u043e\u043b\u0433\u043e\u0442\u0430', db_index=True)),
                ('latitude', models.FloatField(verbose_name='\u0428\u0438\u0440\u043e\u0442\u0430', db_index=True)),
                ('altitude', models.FloatField(null=True, verbose_name='\u0412\u044b\u0441\u043e\u0442\u0430')),
                ('speed', models.FloatField(null=True, verbose_name='\u0421\u043a\u043e\u0440\u043e\u0441\u0442\u044c')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
