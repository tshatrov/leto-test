# -*- coding: utf-8 -*-

import math
from datetime import datetime, timedelta

from django.db import models
from django.db import connection

EVENT_LENGTH = timedelta(seconds=5)
EVENT_TIME_THRESHOLD = timedelta(seconds=60)

SPEED_THRESHOLD = 0.001
EARTH_RADIUS = 6371

def direction_from_delta(delta, latitude):
    phi = math.radians(latitude)
    radius = EARTH_RADIUS * math.cos(phi)
    dx = delta[0]/360.0 * 2 * math.pi * radius
    dy = delta[1]/360.0 * 2 * math.pi * EARTH_RADIUS
    return (dx, dy)

class DriverEvent(models.Model):
    driver_id = models.IntegerField(u'ID водителя', db_index=True)
    status = models.CharField(u'Статус водителя', max_length=32, db_index=True)
    # теоретически этих полей может быть несколько для каждого события, но на практике только одно
    timestamp = models.DateTimeField(u'Время', db_index=True)
    course = models.IntegerField(db_index=True, null=True)
    epoch = models.IntegerField(db_index=True)
    horizontal_accuracy = models.IntegerField(null=True)
    longitude = models.FloatField(u'Долгота', db_index=True)
    latitude = models.FloatField(u'Широта', db_index=True)
    altitude = models.FloatField(u'Высота', null=True)
    speed = models.FloatField(u'Скорость', null=True)
    
    def __unicode__(self):
        return u'%s %s' % (self.driver_id, self.status)

    @classmethod
    def get_drivers(cls):
        u'''Get the list of drivers and their latest status'''
        cursor = connection.cursor()
        cursor.execute("""
SELECT de1.driver_id, de1.timestamp, de1.status
  FROM poll_driverevent de1
  INNER JOIN (SELECT driver_id, MAX(epoch) epoch FROM poll_driverevent GROUP BY driver_id) de2
  ON de1.driver_id = de2.driver_id AND de1.epoch = de2.epoch
ORDER BY de2.epoch DESC;
""")
        return cursor.fetchall()

    @classmethod
    def get_driver_status_data(cls, driver_id, start=None):
        events = cls.objects.filter(driver_id=driver_id).order_by('timestamp')
        if start:
            events = events.filter(timestamp__gte=start)
        cur_status = None, None
        items = []

        def add_item(status, start, end):
            items.append({'driverId': driver_id,
                          'content': status,
                          'start': start.isoformat(),
                          'end': end.isoformat(),
                          'className': 'st-%s' % status.lower(),
                          })
        last_time = None
        for event in events:
            if (last_time and (event.timestamp - last_time) > EVENT_TIME_THRESHOLD):
                add_item(cur_status[0], cur_status[1], last_time + EVENT_LENGTH)
                cur_status = event.status, event.timestamp                
            elif cur_status[0] != event.status:
                if cur_status[0] is not None:
                    add_item(cur_status[0], cur_status[1], event.timestamp)
                cur_status = event.status, event.timestamp
            last_time = event.timestamp
        if cur_status[0] is not None:
            add_item(cur_status[0], cur_status[1], last_time+EVENT_LENGTH)
        return items

    @classmethod
    def get_driver_direction(cls, driver_id, start=None, end=None):
        query = cls.objects.filter(driver_id=driver_id).order_by('-epoch')
        if start:
            query = query.filter(timestamp__gte=start)
        if end:
            query = query.filter(timestamp__lt=end)
        top = list(query[:2])
        if len(top) == 0:
            return None
        elif len(top) == 2:
            if top[0].timestamp - top[1].timestamp > EVENT_TIME_THRESHOLD:
                return None
            vec = (top[0].longitude-top[1].longitude, top[0].latitude-top[1].latitude)
            dirvec = direction_from_delta(vec, top[1].latitude)
            dirveclen = math.sqrt(dirvec[0]*dirvec[0]+dirvec[1]*dirvec[1])
            if dirveclen > SPEED_THRESHOLD:
                return {'pos': {'lon': top[0].longitude,
                                'lat': top[0].latitude},
                        'action': 'moving',
                        'speed': {'x': dirvec[0],
                                  'y': dirvec[1],
                                  }}
        return {'pos': {'lon': top[0].longitude,
                        'lat': top[0].latitude},
                'action': 'still'}
        
