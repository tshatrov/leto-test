import json
from datetime import datetime, timedelta
from PIL import Image, ImageDraw
from StringIO import StringIO

from django.shortcuts import render
from django.http import HttpResponse

from models import DriverEvent

def api_driver_status(request, driver_id):
    data = DriverEvent.get_driver_status_data(driver_id, start=datetime.now()-timedelta(days=30))
    return HttpResponse(json.dumps(data), content_type='application/json')

def driver_timeline(request, driver_id):
    ctx = {'driver_id': driver_id}
    return render(request, 'poll/timeline.html', ctx)

def index(request):
    ctx = {'drivers': DriverEvent.get_drivers()}
    return render(request, 'poll/index.html', ctx)

def api_driver_directions(request):
    drivers = DriverEvent.get_drivers()
    result = []
    for driver_id, timestamp, status in drivers:
        dirinfo = DriverEvent.get_driver_direction(driver_id)
        if dirinfo:
            dirinfo.update({'driverId': driver_id,
                            'status': status,
                            })
            result.append(dirinfo)
    return HttpResponse(json.dumps(result), content_type='application/json')

def driver_map(request):
    ctx = {}
    return render(request, 'poll/map.html', ctx)

status_colors = {
    'Available': ((255, 153, 255, 255), (178, 107, 178, 255)),
    'Dispatched': ((224, 255, 255, 255), (157, 178, 178, 255)),
    'Accepted': ((153, 255, 153, 255), (107, 178, 107, 255)),
    'Rejected': ((255, 77, 77, 255), (178, 54, 54, 255)),
    'Arrived': ((255, 255, 0, 255), (178, 178, 0, 255)),
    'Driving': ((255, 148, 77, 255), (153, 89, 46, 255)),
    'Unavailable': ((184, 184, 184, 255), (110, 110, 110, 255)),
    }

def gen_icon(request, status, action):
    ''' Generates icons for driver map '''
    assert status in status_colors
    assert action in ('moving', 'still')
    im = Image.new('RGBA', (64, 64), (0,0,0,0))
    draw = ImageDraw.Draw(im)
    draw.ellipse([0,0,63,63], fill=status_colors[status][0], outline=status_colors[status][1])

    if action == 'moving':
        draw.polygon(
            [8, 31, 24, 31, 24, 55, 39, 55, 39, 31, 54, 31, 31, 8],
            fill=(0,0,100,255))

    output = StringIO()
    im.save(output, 'PNG')
    return HttpResponse(output.getvalue(), content_type='image/png')
