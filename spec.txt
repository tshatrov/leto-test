
HTTP/1.0 200 OK
Server: LetoTaxi
Connection: keep-alive
Cache-Control: no-cache
Date: Sun, 15 Mar 2015 15:29:14 GMT
Access-Control-Allow-Origin: *
Content-Type: text/event-stream; charset=utf-8

retry: 2000

event: drivers
data: {"driverId": 35680446, "metric": "driverfittedlocations", "fittedLocations": [{"timestamp": "2015-03-01T13:35:57.451000Z", "altitude": 147.748, "longitude": 37.6884658, "course": 49, "epoch": 1425216957451, "horizontalAccuracy": 8, "latitude": 55.7791508, "speed": 4.35836}], "driverStatus": "Arrived"}

retry: 2000

event: drivers
data: {"driverId": 33903722, "metric": "driverfittedlocations", "fittedLocations": [{"timestamp": "2015-03-01T13:36:02.016000Z", "altitude": 154.462, "longitude": 37.5924287, "course": 343, "epoch": 1425216962016, "horizontalAccuracy": 12, "latitude": 55.7409917, "speed": 0.000139544}], "driverStatus": "Accepted"}

retry: 2000

event: drivers
data: {"driverId": 35925908, "metric": "driverfittedlocations", "fittedLocations": [{"timestamp": "2015-03-01T13:36:00.689000Z", "altitude": 150.43, "longitude": 37.6272153, "course": 350, "epoch": 1425216960689, "horizontalAccuracy": 12, "latitude": 55.7224818}], "driverStatus": "Available"}
