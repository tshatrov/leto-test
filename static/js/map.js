
var GLOBAL_MAP;

function initMap() {
    var map = new ol.Map({
        target: 'driver-map',
        layers: [
          new ol.layer.Tile({
            source: new ol.source.MapQuest({layer: 'osm'})
          })
        ],
        view: new ol.View({
          center: ol.proj.transform([ 37.67, 55.77 ], 'EPSG:4326', 'EPSG:3857'),
          zoom: 10
        })
    });
    GLOBAL_MAP = map;
}

function projPoint(lon, lat) {
    return ol.proj.transform([lon, lat], 'EPSG:4326', 'EPSG:3857')
}

// function(){
//             var stillStyle = [new ol.style.Style({
//                 fill: new ol.style.Fill({color: 'navy'}),
//                 stroke: new ol.style.Fill({color: 'black'}),
//             })];

//             var movingStyle = [new ol.style.Style({
//                 fill: new ol.style.Fill({color: 'red'}),
//                 stroke: new ol.style.Fill({color: 'black'}),
//             })];
//             return function(feature, resolution){
//             if (feature.get('action') === 'moving')
//                 return movingStyle;
//             else
//                 return stillStyle;
//             }}()


function getRotation(feature){
    var speed = feature.get('speed');
    if (speed.y === 0) {
        if (speed.x > 0)
            return 90;
        else
            return -90;
    } else {
        return Math.atan2(speed.x,speed.y)*180/Math.PI + 90;
    }
}


function resToScale(resolution) {
    if (resolution < 70) {
        return 0.4;
    } else if (resolution < 150) {
        return 0.3;
    }
    return 0.2
}

function getFeatureStyle(feature, resolution){
    return [new ol.style.Style({
        image: 
        new ol.style.Icon({
            src: '/img/'+feature.get('status')+'_'+feature.get('action')+'.png',
            opacity: (resolution>=150)? 1: 0.7,
            scale: resToScale(resolution),
            rotation: (feature.get('action')==='moving')? getRotation(feature): null
        }),
        text: (resolution<100) ? (new ol.style.Text({
            text: feature.get('id').toString(),
            offsetY: 15,
            fill: new ol.style.Fill({
                color: '#000'
            }),
            stroke: new ol.style.Stroke({
                color: '#fff',
                width: 2
            })
        })) : null
    })];
}


function initDirections(data) {
    var features = [];
    $.each(data, function(){
        var feature = new ol.Feature({
            geometry: new ol.geom.Point(projPoint(this.pos.lon, this.pos.lat)),
            id: this.driverId,
            action: this.action,
            speed: this.speed,
            status: this.status});
        // feature.setStyle(getFeatureStyle(feature));
        features.push(feature);
    });
    var vectorSource = new ol.source.Vector({
        features: features
    });

    var dirLayer = new ol.layer.Vector({
        source: vectorSource,
        style: getFeatureStyle
    });
    GLOBAL_MAP.addLayer(dirLayer);

}

$(document).ready(function() {
    initMap();
    $.get(DIRECTIONS_API_URL, 'json', initDirections);
    });
