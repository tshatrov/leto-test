
function initTimeline(data){
    var container = document.getElementById("timeline-container");
    var items = new vis.DataSet(data);
    var options = {stack:false};

    var timeline = new vis.Timeline(container, items, options);
}


$(document).ready(function() {
    $.get(TIMELINE_API_URL, 'json', initTimeline);
    });
